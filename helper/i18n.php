<?php
function valid($locale) {
    return in_array($locale, ['ca', 'es']);
}

if (isset($_GET['lang']) && valid($_GET['lang'])) {
    $language = $_GET['lang'].'.utf8';
}else{
    $language = 'es.utf8';
}

// indicamos y cambiamos las variables para el idioma seleccionado
putenv("LC_ALL=$language");
putenv("LC_LANG=$language");
setlocale(LC_ALL, $language);

//messages se denomina "domain text" y corresponde al nombre del archivo de traducción.
//locale es el directorio desde el cual se cargarán los archivos de traducción.
$domain = 'messages';
bindtextdomain($domain, "../locale");
textdomain($domain);
?>
