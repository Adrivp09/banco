<?php


class Cuenta
{
    private $cuenta;
    private $creacion;
    private $saldo;

    /**
     * Cuenta constructor.
     * @param $cuenta
     * @param $creacion
     * @param $saldo
     */
    public function __construct($cuenta, $creacion, $saldo)
    {
        $this->cuenta = $cuenta;
        $this->creacion = $creacion;
        $this->saldo = $saldo;
    }

    /**
     * @return mixed
     */
    public function getCuenta()
    {
        return $this->cuenta;
    }

    /**
     * @param mixed $cuenta
     */
    public function setCuenta($cuenta)
    {
        $this->cuenta = $cuenta;
    }

    /**
     * @return mixed
     */
    public function getCreacion()
    {
        return $this->creacion;
    }

    /**
     * @param mixed $creacion
     */
    public function setCreacion($creacion)
    {
        $this->creacion = $creacion;
    }

    /**
     * @return mixed
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param mixed $saldo
     */
    public function setSaldo($saldo)
    {
        $this->saldo = $saldo;
    }


}
?>